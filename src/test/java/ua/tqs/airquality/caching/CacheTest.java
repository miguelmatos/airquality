package ua.tqs.airquality.caching;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CacheTest {

    @Test
    public void testAddRemoveObjects() {
        Cache<String, String> cache = new Cache<>(200);

        cache.put("1", "1");
        cache.put("2", "2");
        cache.put("3", "3");
        cache.put("4", "4");
        cache.put("5", "5");

        assertThat(cache.size()).isEqualTo(5);

        cache.remove("1");

        assertThat(cache.size()).isEqualTo(4);
    }

    @Test
    public void testTimeOut() throws InterruptedException {
        Cache<String, String> cache = new Cache<>(1);

        cache.put("1", "1");
        cache.put("2", "2");

        Thread.sleep(3000);

        assertThat(cache.size()).isEqualTo(0);
    }

    @Test
    void testGet() {
        Cache<String, String> cache = new Cache<>(1000);
        cache.put("1", "1");

        assertThat(cache.get("1")).isEqualTo("1");

        assertThat(cache.get("null")).isNull();
    }
}