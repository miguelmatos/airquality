package ua.tqs.airquality.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ua.tqs.airquality.model.AirQuality;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class AirRepositoryTest {
    AirQuality airQuality;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AirRepository airRepository;

    @BeforeEach
    void setUp() {
        airQuality = new AirQuality();
        airQuality.setCountry("Portugal");
        airQuality.setState("Aveiro");
        airQuality.setCity("Ilhavo");

        airQuality.setTimestamp(new Date());
        airQuality.setTp(15);
        airQuality.setPr(1005);
        airQuality.setHu(72);
        airQuality.setWs(4.6);
        airQuality.setWd(170);
        airQuality.setIc("03d");
    }

    @Test
    void testWhenFindReturnAirQuality() {
        entityManager.persistAndFlush(airQuality);

        AirQuality airFound = airRepository.findByCountryAndStateAndCity("Portugal", "Aveiro", "Ilhavo");

        assertThat(airFound).isEqualTo(airQuality);
    }

    @Test
    void testWhenFindInvalidReturnNull() {
        AirQuality airNotFound = airRepository.findByCountryAndStateAndCity("What", "country", "is this?");
        assertThat(airNotFound).isNull();
    }
}