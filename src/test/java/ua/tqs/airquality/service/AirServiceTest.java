package ua.tqs.airquality.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.tqs.airquality.model.AirQuality;
import ua.tqs.airquality.repository.AirRepository;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AirServiceTest {

    @Mock(lenient = true)
    private AirRepository repository;

    @InjectMocks
    private AirService service;

    @BeforeEach
    void setUp() {
        AirQuality airQuality = new AirQuality();
        airQuality.setCountry("Portugal");
        airQuality.setState("Aveiro");
        airQuality.setCity("Ilhavo");

        airQuality.setTimestamp(new Date());
        airQuality.setTp(15);
        airQuality.setPr(1005);
        airQuality.setHu(72);
        airQuality.setWs(4.6);
        airQuality.setWd(170);
        airQuality.setIc("03d");

        Mockito.when(repository.findByCountryAndStateAndCity("Portugal", "Aveiro", "Ilhavo"))
                .thenReturn(airQuality);
    }

    @Test
    void whenValidCityMustBeFound() {
        AirQuality quality = service.getAirQuality("Portugal", "Aveiro", "Ilhavo");

        assertThat(quality.getCity()).isEqualTo("Ilhavo");
    }

    @Test
    void whenInvalidCityMustNotBeFound() {
        AirQuality quality = service.getAirQuality("This", "city", "doesn't exist");

        assertThat(quality).isNull();
    }
}