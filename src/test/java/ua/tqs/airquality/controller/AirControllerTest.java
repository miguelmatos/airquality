package ua.tqs.airquality.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ua.tqs.airquality.model.AirQuality;
import ua.tqs.airquality.service.AirService;

import java.util.Date;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AirController.class)
class AirControllerTest {

    AirQuality airQuality;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AirService airService;

    @BeforeEach
    void setUp() {
        airQuality = new AirQuality();
        airQuality.setCountry("Portugal");
        airQuality.setState("Aveiro");
        airQuality.setCity("Ilhavo");

        airQuality.setTimestamp(new Date());
        airQuality.setTp(15);
        airQuality.setPr(1005);
        airQuality.setHu(72);
        airQuality.setWs(4.6);
        airQuality.setWd(170);
        airQuality.setIc("03d");
    }

    @Test
    void getAirQualityTest() throws Exception {
        given(airService.getAirQuality("Portugal", "Aveiro", "Ilhavo")).willReturn(airQuality);

        mvc.perform(get("/api/air/Portugal/Aveiro/Ilhavo").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        reset(airService);

    }
}