package ua.tqs.airquality.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.tqs.airquality.model.AirQuality;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface AirRepository extends JpaRepository<AirQuality, Long> {
    AirQuality findByCountryAndStateAndCity(String country, String state, String city);
}
