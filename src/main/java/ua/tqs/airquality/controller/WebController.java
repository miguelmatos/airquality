package ua.tqs.airquality.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ua.tqs.airquality.model.AirQuality;
import ua.tqs.airquality.repository.AirRepository;

@Controller
public class WebController {
    AirQuality airQuality;
    @Autowired
    private AirRepository airRepository;

    public WebController(AirRepository airRepository) {
        this.airRepository = airRepository;
        airQuality = new AirQuality();
        airQuality.setCity("");
        airQuality.setCountry("");
        airQuality.setState("");

        airQuality.setWd(0);
        airQuality.setPr(0);
        airQuality.setTp(0);
        airQuality.setHu(0);
        airQuality.setIc("0");
    }

    @GetMapping("/")
    public String index(Model model) {

        model.addAttribute("city", airQuality);

        return "index";
    }

    @GetMapping("/{country}/{state}/{city}")
    public String get(Model model, @PathVariable String country, @PathVariable String state, @PathVariable String city) {
        AirQuality quality = airRepository.findByCountryAndStateAndCity(country, state, city);
        if (quality == null)
            model.addAttribute("city", airQuality);
        else
            model.addAttribute("city", quality);
        return "index";
    }
}
