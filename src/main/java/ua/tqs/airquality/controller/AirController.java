package ua.tqs.airquality.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import ua.tqs.airquality.caching.Cache;
import ua.tqs.airquality.model.AirQuality;
import ua.tqs.airquality.service.AirService;

@RestController
@RequestMapping("/api")
public class AirController {
    private static final String URL = "https://api.airvisual.com/v2/city?city=%s&state=%s&country=%s&key=862b9fb2-61bf-47b3-bbbc-7e4ececa7236";

    @Autowired
    private AirService airService;

    @Autowired
    private RestTemplate restTemplate;

    private Cache<String, AirQuality> cache = new Cache<>(60);

    public AirController(AirService airService) {
        this.airService = airService;
    }

    @GetMapping("/air/{country}/{state}/{city}")
    public AirQuality getAirQuality(String country, String state, String city) {
        String key = country + "," + state + "," + city;
        if (cache.get(key) != null)
            return cache.get(key);

        return airService.getAirQuality(country, state, city);
    }
}
