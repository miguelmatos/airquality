package ua.tqs.airquality.caching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple cache implementation adapted from https://crunchify.com/how-to-create-a-simple-in-memory-cache-in-java-lightweight-cache/
 *
 * @param <K> key
 * @param <T> value
 */
public class Cache<K, T> {
    private final Map<K, CacheObject<T>> map;
    private long timeToLive;

    public Cache(long timeToLive) {
        this.timeToLive = timeToLive * 1000;
        map = new HashMap<>();

        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.exit(1);
                }
                cleanUp();
            }
        });

        thread.setDaemon(true);
        thread.start();
    }

    public void put(K key, T value) {
        synchronized (map) {
            map.put(key, new CacheObject<>(value));
        }
    }

    public T get(K key) {
        synchronized (map) {
            CacheObject<T> c = map.get(key);

            if (c == null) {
                return null;
            }
            c.lastAccessed = System.currentTimeMillis();
            return c.value;
        }
    }

    public void remove(K key) {
        synchronized (map) {
            map.remove(key);
        }
    }

    public int size() {
        synchronized (map) {
            return map.size();
        }
    }

    private void cleanUp() {
        long now = System.currentTimeMillis();
        ArrayList<K> deleteKey = null;

        synchronized (map) {
            deleteKey = new ArrayList<>();
            CacheObject<T> c;

            for (Map.Entry<K, CacheObject<T>> k : map.entrySet()) {
                c = map.get(k.getKey());

                if (c != null && (now > (timeToLive + c.lastAccessed)))
                    deleteKey.add(k.getKey());
            }
        }

        for (K key : deleteKey) {
            synchronized (map) {
                map.remove(key);
            }

            Thread.yield();
        }

    }

    private static class CacheObject<T> {

        private long lastAccessed = System.currentTimeMillis();
        private T value;

        public CacheObject(T value) {
            this.value = value;
        }
    }
}
