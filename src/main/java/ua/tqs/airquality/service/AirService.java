package ua.tqs.airquality.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.tqs.airquality.model.AirQuality;
import ua.tqs.airquality.repository.AirRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class AirService {
    @Autowired
    private AirRepository airRepository;

    public AirService(AirRepository airRepository) {
        this.airRepository = airRepository;
    }

    public AirQuality getAirQuality(String country, String state, String city) {
        return airRepository.findByCountryAndStateAndCity(country, state, city);
    }
}
